USE [bdPokemon]
GO
/****** Object:  Table [dbo].[datos]    Script Date: 1/12/2021 05:30:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[datos](
	[id] [int] IDENTITY(1,1) NOT NULL,
	[nombre] [varchar](200) NULL,
	[descripcion] [varchar](200) NULL,
	[altura] [varchar](200) NULL,
	[peso] [varchar](200) NULL,
	[categoria] [varchar](200) NULL,
	[habilidad] [varchar](200) NULL,
	[tipo] [varchar](200) NULL,
	[debilidad] [varchar](200) NULL,
	[ruta] [varchar](200) NULL
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[datos] ON 

INSERT [dbo].[datos] ([id], [nombre], [descripcion], [altura], [peso], [categoria], [habilidad], [tipo], [debilidad], [ruta]) VALUES (1, N'Bulbasaur', N'Este Pokémon nace con una semilla en el lomo, que brota con el paso del tiempo', N'0,7 m', N'6,9 kg', N'Semilla', N'Espesura', N'Planta, veneno', N'Fuego, psiquico, volador  y hielo', N'./assets/img/001.jpg')
INSERT [dbo].[datos] ([id], [nombre], [descripcion], [altura], [peso], [categoria], [habilidad], [tipo], [debilidad], [ruta]) VALUES (2, N'Ivysaur', N'Cuando le crece bastante el bulbo del lomo, pierde la capacidad de erguirse sobre las patas traseras.', N'1,0 m', N'13,0 kg', N'Semilla', N'Espesura', N'Planta, veneno', N'Fuego, psiquico, volador  y hielo', N'./assets/img/002.jpg')
INSERT [dbo].[datos] ([id], [nombre], [descripcion], [altura], [peso], [categoria], [habilidad], [tipo], [debilidad], [ruta]) VALUES (3, N'Blastoise', N'Para acabar con su enemigo, lo aplasta con el peso de su cuerpo. En momentos de apuro, se esconde en el caparazón.', N'1,6 m', N'85,5 kg', N'Armazón', N'Torrente', N'Agua', N'Planta y Electrico', N'./assets/img/003.jpg')
SET IDENTITY_INSERT [dbo].[datos] OFF
GO
/****** Object:  StoredProcedure [dbo].[ObtenerDatosPokemon]    Script Date: 1/12/2021 05:30:51 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE procedure [dbo].[ObtenerDatosPokemon]

as

begin
select 
id
,nombre
,descripcion
,altura
,peso
,categoria
,habilidad
,tipo
,debilidad
,ruta
from [dbo].[datos]
end
GO
